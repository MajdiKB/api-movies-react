import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  userFavMovies: [],
};

const {
  actions: {
    setAddFavMovie,
    removeFavMovie,
    resetFavMovie,
  },
  reducer,
  name: favMoviesName
} = createSlice({
  name: 'favMovies',
  initialState,
  reducers: {
    setAddFavMovie: (state, {payload: {movieId, title}}) => {
      return {...state, userFavMovies: [...state.userFavMovies, {movieId, title}] };
    },
    removeFavMovie: (state, {payload}) => {
      const removeFav = state.userFavMovies.filter(item=>item.movieId!==payload);
      return {...state, userFavMovies: removeFav };
    },
    resetFavMovie: () => {
      return initialState;
    },
  }
})

const favMovieSelector = (state) => state[favMoviesName];

export {
  favMoviesName,
  setAddFavMovie,
  removeFavMovie,
  resetFavMovie,
  favMovieSelector,
}

export default reducer;