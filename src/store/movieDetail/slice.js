import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  movieDetail: {
    id: undefined,
    name: undefined,
    img: undefined,
    web: undefined,
    languages: undefined,
    description: undefined,
    status: undefined,
    tagline: undefined,
    adult: undefined,
    countries: [],
    companies: [],
  }
};

const {
  actions: {
    setMovieDetail,
  },
  reducer,
  name: movieDetailName
} = createSlice({
  name: 'movieDetail',
  initialState,
  reducers: {

    setMovieDetail: (state, action) => {
      state.movieDetail = action.payload;
      // return {...state, movieDetail: {
      //   id: action.payload.id,
      //   name: action.payload.title,
      //   img: action.payload.poster_path,
      //   web: action.payload.homepage,
      //   languages: action.payload.original_language,
      //   description: action.payload.overview,
      //   status: action.payload.status,
      //   tagline: action.payload.tagline,
      //   adult: action.payload.adult,
      //   countries: action.payload.production_countries,
      //   companies: action.payload.production_companies,
      //   }
      // }
    },
  }
})

const movieDetailSelector = (state) => state[movieDetailName];

export {
  setMovieDetail,
  movieDetailSelector,
  movieDetailName,
}

export default reducer;