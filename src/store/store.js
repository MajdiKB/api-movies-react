import {combineReducers, configureStore} from '@reduxjs/toolkit';

import auth, {authName} from './auth';
import favMovies, {favMoviesName} from './favMovies';
import pages, { pagesName } from './pages';
import movieDetail, { movieDetailName } from './movieDetail';

const store = configureStore({
  reducer: combineReducers({
    [authName]: auth,
    [favMoviesName]: favMovies,
    [pagesName]: pages,
    [movieDetailName]: movieDetail,
  }),
});

export default store;
