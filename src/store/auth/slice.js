import { createSlice } from '@reduxjs/toolkit';
import {resetFavMovie} from '../favMovies';
import {resetPages} from '../pages';

const initialState = {
  userId: undefined,
  isLoading: false,
  error: undefined,
};

const {
  actions: {
    setLoading,
    setAuthUser,
    setError,
    resetAuthUser,
  },
  reducer,
  name: authName
} = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setLoading: (state, { payload }) => {
      return {...state, isLoading: payload};
    },
    setAuthUser: (state, {payload}) => {
      return {...state, userId: payload, isLoading: false };
    },
    setError: (state, {payload}) => {
      return {...state, error: payload, isLoading: false };
    },
    resetAuthUser: () => {
      return initialState;
    },
  }
});

const resetAll = () => {
  return (dispatch) => {
    dispatch(resetAuthUser());
    dispatch(resetFavMovie());
    dispatch(resetPages());
  }
};

const authSelector = (state) => state[authName];

export {
  authName,
  authSelector,
  setLoading,
  setAuthUser,
  setError,
  resetAuthUser,
  resetAll,
}

export default reducer;