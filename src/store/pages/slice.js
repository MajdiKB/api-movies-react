import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  page: 1,
  totalPages: 0,
};
const {
  actions: {
    setPage,
    setNextPage,
    setPrevPage,
    setFirstPage,
    setLastPage,
    setTotalPages,
    resetPages,
  },
  reducer,
  name: pagesName
} = createSlice({
  name: 'pages',
  initialState,
  reducers: {
    setPage: (state, action) => {
      return {...state, page: action.payload}
    },
    setNextPage: (state, action) => {
      return {...state, page: action.payload + 1}
    },
    setPrevPage: (state, action) => {
      return {...state, page: action.payload - 1}
    },
    setFirstPage: (state) => {
      return {...state, page: 1}
    },
    setLastPage: (state, action) => {
      return {...state, page: action.payload}
    },
    setTotalPages: (state, action) => {
      state.totalPages = action.payload
    },
    resetPages: () => {
      return initialState;
    },
  }
})

const pageSelector = (state) => state[pagesName];

export {
  pageSelector,
  pagesName,
  setPage,
  setNextPage,
  setPrevPage,
  setFirstPage,
  setLastPage,
  setTotalPages,
  resetPages,
}

export default reducer;