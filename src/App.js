import React from 'react';
import routes from  './config/routes';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { Provider } from 'react-redux';

import store from './store'

import NoUserRoute from './components/NoUserRoute';
import SecureRoute from './components/SecureRoute';
import Header from './components/Header';
import Footer from './components/Footer';
import MoviesList from './components/MoviesList';
import MovieDetail from './components/MovieDetail';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';

import './App.scss';

const App = () => {

    return (
    <Provider store={store}>
      <div className="main">
        <Header />
        <Router>
          <Switch>
            <Route exact path={routes.home}>
                <Home/>
            </Route>
            <NoUserRoute exact path={routes.register}>
                <Register/>
            </NoUserRoute>
            <NoUserRoute exact path={routes.login}>
                <Login/>
            </NoUserRoute>
            <SecureRoute exact path={routes.movies}>
                <MoviesList/>
            </SecureRoute>
            <SecureRoute exact path={routes.movieDetail} >
                <MovieDetail/>
            </SecureRoute>
            <Redirect to={routes.home} />
          </Switch>
        </Router>
        <Footer />
      </div>
     </Provider>
    );
}

export default App;
