import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { authSelector, resetAll } from '../../store/auth';
import { pageSelector } from '../../store/pages'
import routes from  '../../config/routes';
import { Link, generatePath } from 'react-router-dom';

import './Home.scss'

const Home = () => {
  //use Dispatch para ejectura las funciones q tenemos en el store
  const dispatch = useDispatch();
  const {userId} = useSelector(authSelector);
  const {page} = useSelector(pageSelector);

  const logOut = () => {
    dispatch(resetAll())
  }

    return(
      <div className="home">
        <nav className="home__nav">
          {userId === undefined && <Link  to={routes.login}><button className="home__nav-btn">LOGIN <span className="fas fa-user"></span></button></Link>}
          {userId === undefined &&  <Link to={routes.register}><button className="home__nav-btn">REGISTER <span className="fas fa-user-plus"></span></button></Link>}
          {userId && <Link to={generatePath(routes.movies, { page: page })}><button className="home__nav-btn">GO TO MOVIES <span className="fas fa-arrow-right"></span></button></Link>}
          {userId && <button onClick={logOut} className="home__nav-btn">LOG OUT <span className="fas fa-sign-out-alt"></span></button>}
        </nav>
        {userId ? <h5>Usuario: {userId}</h5> : null}
      </div>
    )
}

export default Home;