import React from 'react'
import {Link, generatePath} from 'react-router-dom';
import routes from '../../config/routes';
import './FavoritesMovies.scss'
import { useSelector } from 'react-redux';
import { authSelector } from '../../store/auth';
import {favMovieSelector} from '../../store/favMovies';

const FavoritesMovies = () => {
  const {userId}=useSelector(authSelector)
  const {userFavMovies} = useSelector(favMovieSelector);
    return (
      userFavMovies.length>0 ?
      <div className="fav">
        <h1>Favorite Movies</h1>
        <div className="fav__list">
          {userFavMovies.map(element => {
            return(
             <div className="fav__list-items" key={element.movieId}>
             {userId ?
              <Link to={generatePath(routes.movieDetail, { id: element.movieId })}>
                  <p className="fav__list-items-item">{element.title}</p>
              </Link>
              : <p>Usuario no logeado</p> }
             </div>
            )
          })}
        </div>
      </div>
      : null
    )
}

export default FavoritesMovies;
