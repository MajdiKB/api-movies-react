import React from 'react';
import { generatePath} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import FormUsers from '../../components/FormUsers';
import loginUser from '../../api/loginUser'
import {useHistory} from 'react-router-dom';
import routes from '../../config/routes';
import {setAuthUser, setError, setLoading} from '../../store/auth'
import {pageSelector} from "../../store/pages";

const Login = () => {
  const dispatch = useDispatch()
  const history = useHistory();
  const { page } = useSelector(pageSelector);

  const doLogin = async ({email, password}) => {
    try {
      dispatch(setLoading(true));
      const data = await loginUser(email, password);
      dispatch(setAuthUser(data.token));
      history.push(generatePath(routes.movies, {page: page}));
    }catch (error){
      dispatch(setError(error));
      history.push(routes.login);
    }
  }
  return (
    <main>
      <section>
          <FormUsers onSubmitAction={doLogin} typeForm="login" />
      </section>
    </main>
  )
}

export default Login;