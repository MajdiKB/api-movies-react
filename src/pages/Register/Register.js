import React from 'react';
import FormUsers from '../../components/FormUsers';
import registerUser from '../../api/loginUser';
import {useHistory} from 'react-router-dom';
import routes from '../../config/routes';

const Register = () => {
  const history = useHistory();

  const doRegister = async ({email, password}) => {
    const data = await registerUser(email, password);
    if (data.error){
      console.log('Usuario no registrado');
      history.push(routes.register);
    }
    else{
      console.log('Usuario registrado');
      history.push(routes.login);
    }
  }

  return (
    <main>
      <section>
        <FormUsers onSubmitAction={doRegister} typeForm="register" />
      </section>
    </main>
  )
}

export default Register;