import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import routes from "../../config/routes";
import { Link, useParams, generatePath } from "react-router-dom";

import movieDetailApi from "../../api/movieDetail";
import "./MovieDetail.scss";
import { authSelector, resetAll } from "../../store/auth";
import { pageSelector } from "../../store/pages";
import { movieDetailSelector, setMovieDetail } from "../../store/movieDetail";
import {
  favMovieSelector,
  setAddFavMovie,
  removeFavMovie,
} from "../../store/favMovies";

const MovieDetail = () => {
  const dispatch = useDispatch();
  const { userId } = useSelector(authSelector);
  const { userFavMovies } = useSelector(favMovieSelector);
  const { movieDetail } = useSelector(movieDetailSelector);
  const { page } = useSelector(pageSelector);
  // obtenemos el id  con el useParams, para q la app funcione si copiamos la dirección url
  const { id: movieId } = useParams();

  useEffect(() => {
    window.scroll(0, 0);
    getMovieDetail(movieId);
  }, [movieId]);

  const getMovieDetail = (movieId) => {
    movieDetailApi(movieId).then(
      ({
        id,
        title,
        poster_path,
        homepage,
        original_language,
        overview,
        status,
        tagline,
        adult,
        production_countries,
        production_companies,
      }) => {
        dispatch(
          setMovieDetail({
            id,
            name: title,
            img: poster_path,
            web: homepage,
            languages: original_language,
            description: overview,
            status,
            tagline,
            adult,
            countries: production_countries,
            companies: production_companies,
          })
        );
      }
    );
  };

  const selectLanguage = (language) => {
    switch (language) {
      case "en":
        return "English";
      case "es":
        return "Español";
      case "fr":
        return "French";
      case "ko":
        return "Korean";
      case "nl":
        return "Holland";
      case "zh":
        return "Chinesse";
      default:
        return "Unknown language";
    }
  };
  return movieDetail ? (
    <div className="movie-details">
      <div className="movie-details__title">
        <div className="movie-details__nav">
          <Link to={routes.home}>
            <button className="home__nav-btn">
              <span className="fas fa-home"></span>
            </button>
          </Link>
          <Link to={generatePath(routes.movies, { page: page })}>
            <button className="home__nav-btn">
              BACK <span className="fas fa-arrow-right"></span>
            </button>
          </Link>
          <button
            onClick={() => {
              dispatch(resetAll());
            }}
            className="home__nav-btn"
          >
            LOG OUT <span className="fas fa-sign-out-alt"></span>
          </button>
        </div>
      </div>
      <div className="movie-details__list">
        <div className="movie-details__list-img">
          <img
            className="movie-details__list-img-item"
            src={`https://image.tmdb.org/t/p/w500/${movieDetail.img}`}
            alt={movieDetail.name}
          />
        </div>
        <div className="movie-details__list-description">
          <h1 className="movie-details__list-description-item">
            Title: {movieDetail.name}
          </h1>
          <h3 className="movie-details__list-description-item">
            Languages: {selectLanguage(movieDetail.languages)}
          </h3>
          <h3 className="movie-details__list-description-item">
            Description: {movieDetail.description}
          </h3>
          <h3 className="movie-details__list-description-item">
            Status: {movieDetail.status}
          </h3>
          {movieDetail.tagline ? (
            <h3 className="movie-details__list-description-item">
              Tagline: {movieDetail.tagline}
            </h3>
          ) : null}
          {movieDetail.adult === true ? (
            <h3 className="movie-details__list-description-item">
              Adults: Only Adults.
            </h3>
          ) : (
            <h3 className="movie-details__list-description-item">
              Adults: All Public.
            </h3>
          )}
          <h3 className="movie-details__list-description-item">Countries: </h3>
          {movieDetail.countries?.map((item) => {
            return (
              <div
                className="movie-details__list-description-item-countries"
                key={item.name}
              >
                <h4 className="movie-details__list-description-item-array-items">
                  {item.name}.
                </h4>
              </div>
            );
          })}
          <h3 className="movie-details__list-description-item">Companies: </h3>
          {movieDetail.companies?.map((item) => {
            return (
              <div
                className="movie-details__list-description-item-companies"
                key={item.name}
              >
                <h4 className="movie-details__list-description-item-array-items">
                  {item.name}.
                </h4>
                {item.logo_path ? (
                  <img
                    className="movie-details__list-description-item-array-items-img"
                    src={`https://image.tmdb.org/t/p/w500/${item.logo_path}`}
                    alt={item.name}
                  />
                ) : null}
              </div>
            );
          })}
          <h3 className="movie-details__list-description-item">Web: </h3>
          {movieDetail.web ? (
            <span className="movie-details__list-description-item-web">
              <a target="_blank" href={movieDetail.web} rel="noreferrer">
                {movieDetail.web}
              </a>
            </span>
          ) : (
            <span className="movie-details__list-description-item-web">
              NO WEB
            </span>
          )}
          {userFavMovies ? (
            userFavMovies.find((item) => item.movieId === movieDetail.id) ? (
              <span
                className="all-movies__list-item-btn-fav fas fa-trash-alt movie-details__list-description-item"
                onClick={() => {
                  dispatch(removeFavMovie(movieDetail.id));
                }}
              ></span>
            ) : (
              <span
                className="all-movies__list-item-btn-fav fas fa-heart movie-details__list-description-item"
                onClick={() => {
                  dispatch(
                    setAddFavMovie({
                      movieId: movieDetail.id,
                      title: movieDetail.name,
                    })
                  );
                }}
              ></span>
            )
          ) : (
            <span
              className="all-movies__list-item-btn-fav fas fa-heart movie-details__list-description-item"
              onClick={() => {
                dispatch(
                  setAddFavMovie({
                    movieId: movieDetail.id,
                    title: movieDetail.name,
                  })
                );
              }}
            ></span>
          )}
        </div>
      </div>
    </div>
  ) : null;
};

export default MovieDetail;
