import React from 'react';
import './Header.scss'

const Header = () =>{
    return(
      <div className="header">
        <img className="header__logo" src="http://dirz8dubrwck5.cloudfront.net/wp-content/uploads/sites/31/2018/03/02152409/Fall-Movie-Review.jpg" alt="LOGO"/>
        <h1 className="header__title">App Movies</h1>
      </div>
    )
}

export default Header;