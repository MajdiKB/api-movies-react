import React from 'react';
import './Footer.scss'

const Footer = () => {
    return(
      <div className="footer">
        <h5>Todos los derechos reservados. Majdi Kokaly - Upgrade Hub</h5>
      </div>
    )
}

export default Footer;