import React, { useState } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import routes from "../../config/routes";

import "./FormUsers.scss";

const emptyUser = {
  email: "",
  password: "",
};

const FormUsers = ({ onSubmitAction, typeForm }) => {
  const [userFormData, setUserFormData] = useState(emptyUser);

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    onSubmitAction(userFormData);
  };

  const handleChangeInput = (event) => {
    setUserFormData((prevValue) => ({
      ...prevValue,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <div>
      <form onSubmit={handleFormSubmit}>
        <div className="form">
          <div>
            <label htmlFor="email">
              <input
                className="form__input"
                name="email"
                type="email"
                placeholder="EMAIL"
                value={userFormData.email}
                onChange={handleChangeInput}
              />
            </label>
          </div>
          <div>
            <label htmlFor="password">
              <input
                className="form__input"
                name="password"
                type="password"
                placeholder="PASSWORD"
                value={userFormData.password}
                onChange={handleChangeInput}
              />
            </label>
          </div>
          <div>
            {typeForm === "register" ? (
              <button className="home__nav-btn" type="submit">
                Register
              </button>
            ) : (
              <button className="home__nav-btn" type="submit">
                Login
              </button>
            )}
            <Link to={routes.home}>
              <button className="home__nav-btn">
                <span className="fas fa-home"></span>
              </button>
            </Link>
          </div>
        </div>
      </form>
    </div>
  );
};

FormUsers.propTypes = {
  typeForm: PropTypes.string.isRequired,
  onSubmitAction: PropTypes.func.isRequired,
};

export default FormUsers;
