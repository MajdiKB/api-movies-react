import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import routes from '../../config/routes';
import {authSelector} from '../../store/auth'

const SecureRoute = (props) =>{
    const {userId} = useSelector(authSelector);
    const { ...routeProps } = props;
    return userId ? <Route {...routeProps} /> : <Redirect to={routes.home}/>
  }

  export default SecureRoute;