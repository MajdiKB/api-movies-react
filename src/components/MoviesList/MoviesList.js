import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import moviesListApi from "../../api/movieList";
import FavoritesMovies from "../../pages/FavoritesMovies";
import routes from "../../config/routes";
import { generatePath, Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { authSelector, resetAll } from "../../store/auth";
import {
  favMovieSelector,
  setAddFavMovie,
  removeFavMovie,
} from "../../store/favMovies";
import {
  pageSelector,
  setPage,
  setNextPage,
  setPrevPage,
  setLastPage,
  setFirstPage,
  setTotalPages,
} from "../../store/pages";
import("./MoviesList.scss");

const MoviesList = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { userId } = useSelector(authSelector);
  const { userFavMovies } = useSelector(favMovieSelector);
  const { page, totalPages } = useSelector(pageSelector);
  const [movies, setMovies] = useState();

  const { page: paramsPage } = useParams();

  const getData = () => {
    moviesListApi(paramsPage).then((response) => {
      setMovies(response.results);
      dispatch(setPage(response.page));
      dispatch(setTotalPages(response.total_pages));
    });
  };

  useEffect(() => {
    getData();
  }, [paramsPage]);

  const logOut = () => {
    dispatch(resetAll());
  };

  const nextPage = () => {
    dispatch(setNextPage(page));
  };

  const prevPage = () => {
    dispatch(setPrevPage(page));
  };
  const firstPage = () => {
    dispatch(setFirstPage());
  };
  const lastPage = () => {
    dispatch(setLastPage(totalPages));
  };

  const removeMovieFav = (movieId) => {
    dispatch(removeFavMovie(movieId));
  };

  const addFavMovie = (movieId, title) => {
    dispatch(
      setAddFavMovie({
        movieId: movieId,
        title: title,
      })
    );
  };

  const goToPage = (event) => {
    let { value } = event.target;
    if (value <= totalPages && value >= 1) {
      history.push(generatePath(routes.movies, { page: value }));
    } else {
      event.target.value = "";
    }
  };

  return movies ? (
    <div className="all-movies">
      {userFavMovies ? <FavoritesMovies /> : null}
      <div className="all-movies__home">
        <div className="all-movies__home-buttons">
          {userId ? (
            <Link to={routes.home}>
              <button className="home__nav-btn">
                <span className="fas fa-home"></span>
              </button>
            </Link>
          ) : (
            <p>No user logged</p>
          )}
          <button onClick={logOut} className="home__nav-btn">
            LOG OUT <span className="fas fa-sign-out-alt"></span>
          </button>
        </div>
      </div>

      <nav className="all-movies__nav-menu">
        <div className="all-movies__nav-menu-gotopage-container">
          <h3 className="all-movies__nav-menu-item-title">
            Page: {page} of {totalPages}
          </h3>
          <input
            onChange={goToPage}
            className="all-movies__nav-menu-gotopage-container-input"
            type="number"
            min="1"
            max={totalPages}
            placeholder="go to page..."
          ></input>
          <div>
            {page !== 1 && (
              <Link to={generatePath(routes.movies, { page: 1 })}>
                <button
                  onClick={firstPage}
                  className="all-movies__nav-menu-item"
                >
                  <span className="fas fa-backward"></span>
                </button>
              </Link>
            )}
            {page > 1 && (
              <Link to={generatePath(routes.movies, { page: page - 1 })}>
                <button
                  onClick={prevPage}
                  className="all-movies__nav-menu-item"
                >
                  <span className="fas fa-step-backward"></span>
                </button>
              </Link>
            )}
            {page < totalPages && (
              <Link to={generatePath(routes.movies, { page: page + 1 })}>
                <button
                  onClick={nextPage}
                  className="all-movies__nav-menu-item"
                >
                  <span className="fas fa-step-forward"></span>
                </button>
              </Link>
            )}
            {page !== totalPages && (
              <Link to={generatePath(routes.movies, { page: totalPages })}>
                <button
                  onClick={lastPage}
                  className="all-movies__nav-menu-item"
                >
                  <span className="fas fa-forward"></span>
                </button>
              </Link>
            )}
          </div>
        </div>
      </nav>

      <div className="all-movies__list">
        {movies.map((movieItem) => (
          <div className="all-movies__list-item" key={movieItem.id}>
            <div className="all-movies__list-item-fav">
              <h3 className="all-movies__list-item-title">{movieItem.title}</h3>
              {userFavMovies ? (
                userFavMovies.find((item) => item.movieId === movieItem.id) ? (
                  <span
                    className="all-movies__list-item-btn-fav fas fa-trash-alt movie-details__list-description-item"
                    onClick={() => {
                      removeMovieFav(movieItem.id);
                    }}
                  ></span>
                ) : (
                  <span
                    className="all-movies__list-item-btn-fav fas fa-heart movie-details__list-description-item"
                    onClick={() => {
                      addFavMovie(movieItem.id, movieItem.title);
                    }}
                  ></span>
                )
              ) : (
                <span
                  className="all-movies__list-item-btn-fav fas fa-heart movie-details__list-description-item"
                  onClick={() => {
                    addFavMovie(movieItem.id, movieItem.title);
                  }}
                ></span>
              )}
            </div>
            {userId ? (
              <Link to={generatePath(routes.movieDetail, { id: movieItem.id })}>
                {movieItem.backdrop_path !== null ? (
                  <img
                    className="all-movies__list-item-img"
                    src={`https://image.tmdb.org/t/p/w500/${movieItem.backdrop_path}`}
                    alt={movieItem.title}
                  />
                ) : (
                  <img
                    className="all-movies__list-item-noimg"
                    src={`https://st4.depositphotos.com/14953852/22772/v/600/depositphotos_227725020-stock-illustration-no-image-available-icon-flat.jpg`}
                    alt={movieItem.title}
                  />
                )}
              </Link>
            ) : (
              <p>No user logged</p>
            )}
          </div>
        ))}
      </div>
      {userId ? ( <h5 className="user-bottom">Usuario: {userId}</h5>) : <h5 className="user-bottom">"No user logged"</h5>}
    </div>
  ) : null;
};

export default MoviesList;
