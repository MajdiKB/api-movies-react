import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import routes from  '../../config/routes';
import {authSelector} from '../../store/auth'

const NoUserRoute = ({...routeProps}) =>{
    //usar useSelector para leer datos del store
    const {userId} = useSelector(authSelector);
    return !userId ? <Route {...routeProps} /> : <Redirect to={routes.home}/>
}

export default NoUserRoute;