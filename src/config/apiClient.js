const apiClient = (url, config) => {
  return fetch(url, config).then(response => response.json());
};

export default apiClient;