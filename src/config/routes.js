const home = '/';
const movies = `${home}movies/page/:page`
const movieDetail = `${home}movies/:id`

const register = `${home}register`
const login = `${home}login`

const routes = {
  home,
  movies,
  movieDetail,
  register,
  login,
}

export default routes;