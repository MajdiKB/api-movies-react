const apiMovie = async (url) => {
  return await fetch(url)
    .then(response =>
      response.json());
};
export default apiMovie;