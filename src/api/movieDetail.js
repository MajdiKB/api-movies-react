import apiMovie from '../config/apiMovies';

const apiMovieDetailUrl = "https://api.themoviedb.org/3/movie/";
const apiKey = "8f68c315d0d1a84d75d2524437901b75";

const movieDetailApi = (id) => {
  return apiMovie(`${apiMovieDetailUrl}${id}?api_key=${apiKey}`)
}

export default movieDetailApi;