import apiClient from "../config/apiClient";

const registerUrl = "https://reqres.in/api/register";

const registerUser = async (userData) => {
  const response = await apiClient(registerUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userData),
  });

  const jsonResponse = await response.json();
  if (response.error) {
    alert("Error al registrar usuario.");
    throw new Error("Error al registrar usuario.");
  }
  console.log("Usuario registrado en api: ", jsonResponse.token);
  return jsonResponse;
};

export default registerUser;
