import apiClient from "../config/apiClient";

const loginUrl = "https://reqres.in/api/login";

const loginUser = async (email, password) => {
  const userData = { email, password };
  const response = await apiClient(loginUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userData),
  });
  if (response.error) {
    alert(
      "Usari@ ó contraseña no válid@. { INFO - Usuario: george.edwards@reqres.in Contraseña: any }"
    );
  }
  return response;
};

export default loginUser;
