import apiMovie from '../config/apiMovies';

const apiKey = "8f68c315d0d1a84d75d2524437901b75";
const apiMoviesListUrl = "https://api.themoviedb.org/3/movie/popular?api_key="

const moviesListApi = async (page) => {
  return await apiMovie(`${apiMoviesListUrl}${apiKey}&page=${page}`);
}


export default  moviesListApi;